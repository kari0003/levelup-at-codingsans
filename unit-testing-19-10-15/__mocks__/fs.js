const fs = jest.genMockFromModule('fs');

fs.readdirSync = () => ['kek', 'bur', 'baz'];

module.exports = fs;
