// mock class
// mock function
// mock date.now / math.random
// mock your mom
// mock globally
import { Sitcom } from './mocking';
import { RandomProvider, RandomProviderImplementation } from './mock-dependencies';
import { readdirSync } from 'fs';

jest.mock('fs');

class RandomGeneratorMock implements RandomProvider {
  public getRandom() {
    return 420;
  }
}

describe('Mocking', () => {
  describe('Class', () => {
    test('should use mocked dependency to our advantage', () => {
      const mockDependency = new RandomGeneratorMock();
      const funnySitcom = new Sitcom(mockDependency);

      const joke = funnySitcom.getJoke();

      expect(joke).toEqual(420);
    });
  });

  describe('Spying', () => {
    test('should be able to check call properties', () => {
      const funnierSitcom = new Sitcom(new RandomProviderImplementation());
      const jokeSpy = jest.spyOn(funnierSitcom, 'getJoke').mockImplementation(() => 69);

      const joke = funnierSitcom.getJoke();

      expect(joke).toEqual(69);
      expect(jokeSpy).toHaveBeenCalled();
    });
  });

  describe('Node module', () => {
    test('should mock fs in the __mocks__ folder', () => {
      const mockedDirectories = readdirSync('any path literally');
      expect(mockedDirectories).toEqual(['kek', 'bur', 'baz']);
    });
  });
});
