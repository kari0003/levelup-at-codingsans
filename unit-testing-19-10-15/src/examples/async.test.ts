async function asyncConsole(message: string) {
  return new Promise(resolve =>
    setTimeout(() => {
      console.log(message);
      resolve();
    }),
  );
}

describe('Async tests', () => {
  test('should fail', async done => {
    setTimeout(() => {
      console.log('set timeout executed');
      done();
    });
  });

  test('async await', async done => {
    await asyncConsole('await executed');
    done();
  });

  test('async return promise', async () => {
    return asyncConsole('returning promise executed');
  });

  afterEach(() => console.log('> test done'));
});
