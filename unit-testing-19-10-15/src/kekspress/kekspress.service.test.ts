import { getNextStop } from './kekspress.service';

describe('Kekspress', () => {
  describe('getNextStop', () => {
    // test('should use default skipping value', () => {
    //   expect(getNextStop(1)).toEqual(2);
    // });
    test('should use given skipping value', () => {
      expect(getNextStop(1, 4)).toEqual(6);
    });
  });

  describe('filterStop', () => {});
});
