import { UnrustlerToggleComponent } from '../app/unrustler-toggle/unrustler-toggle.component';

export default {
  title: 'UnrustlerToggleComponent'
};

export const defaultIsAKeyword = () => ({
  component: UnrustlerToggleComponent
});

export const unrustled = () => ({
  component: UnrustlerToggleComponent,
  props: {
    jimmies: -37
  }
});

unrustled.story = {
  parameters: {
    notes:
      'This case is about the wonderful event when your jimmies are unrustled. No need to be upset now!'
  }
};
