import { storiesOf } from '@storybook/angular';

import { UnrustlerToggleComponent } from '../app/unrustler-toggle/unrustler-toggle.component';

storiesOf('UnrustlerToggleComponentOld', module)
  .add('default', () => ({
    component: UnrustlerToggleComponent
  }))
  .add(
    'A Lot of Jimmies are rustled',
    () => ({
      component: UnrustlerToggleComponent,
      props: {
        jimmies: 10000
      }
    }),
    {
      notes:
        'When life gets you hard, a lot of jimmies are rustled. This is a story about you.'
    }
  );
