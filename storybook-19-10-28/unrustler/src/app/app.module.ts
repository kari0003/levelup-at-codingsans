import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UnrustlerToggleComponent } from './unrustler-toggle/unrustler-toggle.component';

@NgModule({
  declarations: [
    AppComponent,
    UnrustlerToggleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
