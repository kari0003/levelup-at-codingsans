import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-unrustler-toggle',
  templateUrl: './unrustler-toggle.component.html',
  styleUrls: ['./unrustler-toggle.component.scss']
})
export class UnrustlerToggleComponent implements OnInit {
  @Input()
  jimmies = 0;

  constructor() {}

  ngOnInit() {}
}
