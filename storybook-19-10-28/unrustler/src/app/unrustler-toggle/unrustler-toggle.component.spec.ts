import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnrustlerToggleComponent } from './unrustler-toggle.component';

describe('UnrustlerToggleComponent', () => {
  let component: UnrustlerToggleComponent;
  let fixture: ComponentFixture<UnrustlerToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnrustlerToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnrustlerToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
