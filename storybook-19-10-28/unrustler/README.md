# Unrustler

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.3.

## Nehézségek

I could not find a quick way to generate stories for my angular component. It would have been good to have a cli function for it, or for it to be added to Angular automatically.

New export story makes it hard to have descriptive story names - eg my story name is a reserved keyword
