# Functional programming

Transform an array of `Item` objects into the reversed structure `ItemReverse`

```typescript
interface Item {
  id: string;
  parent?: Item;
}
```

```typescript
interface ItemReverse {
  id: string;
  children: ItemReverse[];
}
```

## The Task
Extend the code found under `src/reverse-traverse.ts`, to pass the lint and tests done by `yarn test`.
You can use `ramda`, `lodash` or the built in Array and Object methods aswell as destructuring to complete the task, the main goal is to use pure functions, no mutations and no variable declarations within functions (functions can be declared). Try to split your code into atomic functions. The code does not have to be the most optimised, concentrate on creating a readable structure, even if it means more CPU cycles :).

### The algorithm does not need to work on cyclic graphs.

E.g. Input

```typescript
const items: Item[] = [{ id: '0', parent: { id: '3' } }, { id: '2', parent: { id: '1', parent: { id: '3' } } }];
```

E.g. Output

```typescript
const result: ItemReverse = [
  {
    id: '3',
    children: [
      {
        id: '0',
        children: [],
      },
      {
        id: '1',
        children: [
          {
            id: '2',
            children: [],
          },
        ],
      },
    ],
  },
];
```
