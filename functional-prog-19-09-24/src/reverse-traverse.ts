export interface ItemReverse {
  id: string;
  children: ItemReverse[];
}

export interface Item {
  id: string;
  parent?: Item;
}

export interface ItemsDirectory {
  [key: string]: Item[];
}

function veryComplexCalculationToSeeIfParentIsExistingAndIfParentIsTheParentWeAreLookingFor(
  elem: Item,
  parentId: string,
): boolean {
  return !elem.parent || elem.parent.id === parentId;
}

function getFirstChild(elem: Item, parentId: string): string {
  return veryComplexCalculationToSeeIfParentIsExistingAndIfParentIsTheParentWeAreLookingFor(elem, parentId)
    ? elem.id
    : getFirstChild(elem.parent!, parentId);
}

const splitForParent = (list: Item[], id: string): ItemsDirectory =>
  //eslint-disable-next-line complexity
  list.reduce((acc: ItemsDirectory, elem: Item) => {
    if (elem.id === id) {
      return acc;
    }
    acc[getFirstChild(elem, id)] = [...(acc[getFirstChild(elem, id)] || []), elem];
    return acc;
  }, {});

const buildNode = (itemList: Item[], id: string): ItemReverse => ({
  id: id,
  children: Object.entries(splitForParent(itemList, id)).map(([childId, childList]) => buildNode(childList, childId)),
});

export const reverseTraverse = (items: Item[]): ItemReverse[] => buildNode(items, '').children;
