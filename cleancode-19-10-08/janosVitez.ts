import { printLn, breakPoemLines, randomInt } from './utils';

function isEligibleForRepeta(seed: number): boolean {
  return seed < 7 && seed > 1 && seed % 2 == 0;
}

function randomRepeta(poemLines: string) {
  // Number based Janos Vitez you got m8
  const seed = randomInt(10);
  // compute x modulo 2 and check whether it is zero, log the results
  if (seed % 2 == 0) {
    /* the number is even */
    console.log('The number is even', seed);
  } else {
    /* the number is even */
    console.log('The number is odd', seed);
  }

  // Don't know why even numbers between 1 and 7 but it was told to do it like this
  if (isEligibleForRepeta(seed)) {
    console.log('You are lucky, here is Janos Vitez one more time, nicely.');
    printLn(poemLines);
  }
}

export function janosVitez(poem: string) {
  printLn('Hello there, I am your homework, make me better ;)');

  const poemLines = breakPoemLines(poem);
  printLn(poemLines);

  randomRepeta(poemLines);

  console.log('\nGodbye');
}
